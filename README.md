# SubmoduleDemo

## Add
```
git submodule add https://github.com/chaconinc/DbConnector

git commit -am 'Add submodule'

git push origin master
```

## Clone
```
git clone --recurse-submodules https://gitlab.com/duclmdev/submoduledemo
```


## Update submodule
```
git submodule update --init --recursive
```
