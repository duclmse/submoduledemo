plugins {
    java
}

repositories { mavenCentral() }

dependencies {
    implementation(project(":module.core"))
}
